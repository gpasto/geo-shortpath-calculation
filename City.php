<?php
/**
 * @copyright   Grace Pastorelly Ruiz
 * @package     GeoShortPathCalculation
 * @subpackage
 * @category	Model
 * @author      Grace Pastorelly Ruiz <gpasto@gmail.com>
 * @since       2018/10/24
 **/
namespace shortpath;

class City
{
	private $name;
	private $latitude;
	private $longitude;

	public function __toString()
	{
		return $this->name;
	}

	public function setName($value)
	{
		$this->name = trim($value);
		return $this;
	}
	public function getName()
	{
		return $this->name;
	}

	public function setLatitude($value)
	{
		$this->latitude = $value;
		return $this;
	}
	public function getLatitude()
	{
		return $this->latitude;
	}

	public function setLongitude($value)
	{
		$this->longitude = $value;
		return $this;
	}
	public function getLongitude()
	{
		return $this->longitude;
	}
}
