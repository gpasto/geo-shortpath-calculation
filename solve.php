<?php
/**
 * @copyright   Grace Pastorelly Ruiz
 * @package     GeoShortPathCalculation
 * @subpackage
 * @category
 * @author      Grace Pastorelly Ruiz <gpasto@gmail.com>
 * @since       2018/10/25
 **/
namespace shortpath;

include 'shortPath.php';

$geoShortPath = new shortPath();
$geoShortPath->calculateShortPath();
