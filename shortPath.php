<?php
/**
 * @copyright   Grace Pastorelly Ruiz
 * @package     GeoShortPathCalculation
 * @subpackage
 * @category
 * @author      Grace Pastorelly Ruiz <gpasto@gmail.com>
 * @since       2018/10/24
 **/
namespace shortpath;

include 'City.php';

class shortPath
{
	private $rawCities = array();
	private $cities = array();

	/**
	* Calculte short path between cities and print the cities' name in order to visit; start point: Beijing
	**/
	public function calculateShortPath()
	{
		try {
			$this->readCities();

			while (count($this->rawCities) > 0) {
				$currentCity = $this->cities[count($this->cities) - 1];
				$prevDist = 99999999999999999999999999999;
				$i = 0;

				foreach ($this->rawCities as $key => $rawCity) {
					$dist = $this->calculateDistance($currentCity, $rawCity);

					if ($dist < $prevDist) {
						$prevDist = $dist;
						$city = $rawCity;
						$i = $key;
					}
				}

				$this->cities[] = $city;
				unset($this->rawCities[$i]);
			}

			$this->printShortPath();
		} catch (\Exception $e) {
			echo $e->getMessage() . "\n";
		}
	}

	/**
	* Print in standard outputh the names of the cities to visit
	*/
	private function printShortPath()
	{
		foreach ($this->cities as $city) {
			echo $city->getName() . "\n";
		}
	}

	/**
	* Read cities from cities.txt file
	* Start city 'Beijing' is set in cities, others in rawCities
	**/
	private function readCities()
	{
		$fileName = 'cities.txt';

		if (file_exists($fileName)) {
			$rawCities = file($fileName, FILE_SKIP_EMPTY_LINES);

			foreach ($rawCities as $rawCity) {
				$rawCity = explode("\t", $rawCity); // each city field is separated by tab

				if (count($rawCity) > 2) {
				    $cityName = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', implode(' ', array_slice($rawCity, 0, -2)));
					$city = new City();
					$city->setName($cityName); // name
					$city->setLatitude(count($rawCity)-2); //latitude
					$city->setLongitude($rawCity[count($rawCity)-1]); //longitude

					if ($cityName == 'Beijing') {
						$this->cities[] = $city;
					} else {
						$this->rawCities[] = $city;
					}
				} else {
					//var_dump($rawCity);
				}
			}
		} else {
			throw new \Exception('cities.txt file does not exist.', 400);
		}

		return $this;
	}

	/**
	* Calsulate distance between 2 geographical point with 2 decimals
	*
	* @param City $ini
	* @param City $fin
	* @return float
	**/
	private function calculateDistance($ini, $fin)
	{
		$dLat = deg2rad($fin->getLatitude() - $ini->getLatitude());
		$dLon = deg2rad($fin->getLongitude() - $ini->getLongitude());
		$a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($ini->getLatitude())) * cos(deg2rad($fin->getLatitude())) * sin($dLon/2) * sin($dLon/2);
		$c = 2 * atan2(sqrt($a), sqrt(1 - $a));
		$d = 6371 * $c; // in km
		return round($d, 2);
	}
}
